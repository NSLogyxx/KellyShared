//
//  WPNUXUtility.h
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WPNUXUtility : NSObject

+ (UIFont *)textFieldFont;
+ (UIFont *)descriptionTextFont;
+ (UIFont *)titleFont;
+ (UIFont *)swipeToContinueFont;
+ (UIFont *)tosLableFont;
+ (UIFont *)confirmationLabelFont;
+ (UIFont *)tosLabelSmallerFont;

+ (UIColor *)bottomPanelLineColor;
+ (UIColor *)descriptionTextColor;
+ (UIColor *)bottomPanelBackgroundColor;
+ (UIColor *)swipeToContinueTextColor;
+ (UIColor *)confirmationLabelColor;
+ (UIColor *)backgroundColor;
+ (UIColor *)tosLabelColor;

+ (void)centerViews:(NSArray *)controls withStartingView:(UIView *)startingView andEndingView:(UIView *)endingView forHeight:(CGFloat)viewHeight;
+ (void)configurePageControlTintColors:(UIPageControl *)pageControl;
+ (NSDictionary *)titleAttributesWithColor:(UIColor *)color;

@end

NS_ASSUME_NONNULL_END
