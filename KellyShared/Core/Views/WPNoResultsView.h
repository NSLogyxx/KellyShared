//
//  WPNoResultsView.h
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class WPNoResultsView;
@protocol WPNoResultsViewDelegate <NSObject>

@optional
- (void)didTapNoResultsView:(WPNoResultsView *)noResultsView;
@end

@interface WPNoResultsView : UIView

@property(nonatomic, strong) NSString *titleText;
@property(nonatomic, strong) NSString *messageText;
@property(nonatomic, strong) NSString *buttonTitle;
@property(nonatomic, strong) UIView *accessoryView;
@property(nonatomic, strong) UIButton *button;
@property(nonatomic, weak) id<WPNoResultsViewDelegate> delegate;

@property(nonatomic, strong) NSAttributedString *attributedMessageText;

+ (instancetype)noResultsViewWithTitle:(NSString *)titleText message:(NSString *)messageText accessoryView:(UIView *)accessoryView buttonTitle:(NSString *)buttonTitle;
- (void)showInView:(UIView *)view;
- (void)centerInSuperview;

@end

NS_ASSUME_NONNULL_END
