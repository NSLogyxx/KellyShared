//
//  WPTextFieldTableViewCell.h
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class WPTextFieldTableViewCell;

@protocol WPTextFieldTableViewCellDelegate <NSObject>

- (void)cellWantsToSelectNextField:(WPTextFieldTableViewCell *)cell;
@optional
- (void)cellTextDidChange:(WPTextFieldTableViewCell *)cell;

@end

@interface WPTextFieldTableViewCell : WPTableViewCell

@property (nonatomic, strong, readonly) UITextField *textField;
@property (nonatomic, assign) BOOL shouldDismissOnReturn;
@property (nonatomic, weak) id<WPTextFieldTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
