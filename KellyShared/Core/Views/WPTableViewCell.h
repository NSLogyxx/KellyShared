//
//  WPTableViewCell.h
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WPTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL forceCustomCellMargins;

@end

NS_ASSUME_NONNULL_END
