//
//  WPStyleGuide+DynamicType.swift
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

extension WPStyleGuide {
    static let defaultTableViewRowHeight: CGFloat = 44.0
    @objc public static let maxFontSize: CGFloat = 32.0
    
    @objc public class func configureAutomaticHeightRows(for tableView: UITableView) {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = defaultTableViewRowHeight
    }
    
    @objc public class func configureLabel(_ label: UILabel, textStyle style: UIFont.TextStyle) {
        label.font = fontForTextStyle(style)
        label.adjustsFontForContentSizeCategory = true
    }
    
    @objc public class func configureLabel(_ label: UILabel, textStyle style: UIFont.TextStyle, symbolicTraits traits: UIFontDescriptor.SymbolicTraits) {
        label.font = fontForTextStyle(style, symbolicTraits: traits)
        label.adjustsFontForContentSizeCategory = true
    }
    
    @objc public class func configureLabel(_ label: UILabel, textStyle style: UIFont.TextStyle, fontWeight weight: UIFont.Weight) {
        label.font = fontForTextStyle(style, fontWeight: weight)
        label.adjustsFontForContentSizeCategory = true
    }
    
    @objc public class func configureLabelForNotoFont(_ label: UILabel, textStyle style: UIFont.TextStyle) {
        label.font = notoFontForTextStyle(style)
        label.adjustsFontForContentSizeCategory = true
    }
    
    @objc public class func fontForTextStyle(_ style: UIFont.TextStyle, maximumPointSize: CGFloat = maxFontSize) -> UIFont {
        if #available(iOS 11, *) {
            let fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
            let fontToGetSize = UIFont(descriptor: fontDescriptor, size: CGFloat(0))
            return UIFontMetrics(forTextStyle: style).scaledFont(for: fontToGetSize, maximumPointSize: maximumPointSize)
        }
        
        let scaledFontDescriptor = fontDescriptor(style, maximumPointSize: maximumPointSize)
        return UIFont(descriptor: scaledFontDescriptor, size: CGFloat(0))
    }
    
    @objc public class func fontForTextStyle(_ style: UIFont.TextStyle, symbolicTraits traits: UIFontDescriptor.SymbolicTraits, maximumPointSize: CGFloat = maxFontSize) -> UIFont {
        var descriptor = fontDescriptor(style, maximumPointSize: maximumPointSize)
        descriptor = descriptor.withSymbolicTraits(traits) ?? descriptor
        return UIFont(descriptor: descriptor, size: CGFloat(0))
    }
    
    private class func fontDescriptor(_ style: UIFont.TextStyle, maximumPointSize: CGFloat = maxFontSize) -> UIFontDescriptor {
        let fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        let fontToGetSize = UIFont(descriptor: fontDescriptor, size: CGFloat(0))
        let scaledFontSize = CGFloat.minimum(fontToGetSize.pointSize, maximumPointSize)
        return fontDescriptor.withSize(scaledFontSize)
    }
    
    @objc public class func fontForTextStyle(_ style: UIFont.TextStyle, fontWeight weight: UIFont.Weight) -> UIFont {
        var fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        #if swift(>=4.0)
        let traits = [UIFontDescriptor.TraitKey.weight: weight]
        fontDescriptor = fontDescriptor.addingAttributes([.traits: traits])
        #else
        let traits = [UIFontWeightTrait: weight]
        fontDescriptor = fontDescriptor.addingAttributes([UIFontDescriptorTraitsAttribute: traits])
        #endif
        let font = UIFont(descriptor: fontDescriptor, size: CGFloat(0))
        return font
    }
    
    @objc public class func fontSizeForTextStyle(_ style: UIFont.TextStyle) -> CGFloat {
        let fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        let font = UIFont(descriptor: fontDescriptor, size: CGFloat(0))
        return font.pointSize
    }
    
    @objc public class func fixedFont(for style: UIFont.TextStyle, weight: UIFont.Weight = .regular) -> UIFont {
        let defaultContentSizeCategory = UITraitCollection(preferredContentSizeCategory: .large)
        let fontSize = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style, compatibleWith: defaultContentSizeCategory).pointSize
        return UIFont.systemFont(ofSize: fontSize, weight: weight)
    }
    
    @objc public class func notoFontForTextStyle(_ style: UIFont.TextStyle) -> UIFont {
        return customNotoFontNamed("NotoSerif", forTextStyle: style)
    }
    
    @objc public class func notoBoldFontForTextStyle(_ style: UIFont.TextStyle) -> UIFont {
        return customNotoFontNamed("NotoSerif-Bold", forTextStyle: style)
    }
    
    @objc public class func notoItalicFontForTextStyle(_ style: UIFont.TextStyle) -> UIFont {
        return customNotoFontNamed("NotoSerif-Italic", forTextStyle: style)
    }
    
    @objc public class func notoBoldItalicFontForTextStyle(_ style: UIFont.TextStyle) -> UIFont {
        return customNotoFontNamed("NotoSerif-BoldItalic", forTextStyle: style)
    }
    
    private class func customNotoFontNamed(_ fontName: String, forTextStyle style: UIFont.TextStyle, maximumPointSize: CGFloat = maxFontSize) -> UIFont {
        WPFontManager.loadNotoFontFamily()
        let descriptor = fontDescriptor(style, maximumPointSize: maximumPointSize)
        
        guard let font = UIFont(name: fontName, size: descriptor.pointSize) else {
            return fontForTextStyle(style)
        }
        return font
    }
}
