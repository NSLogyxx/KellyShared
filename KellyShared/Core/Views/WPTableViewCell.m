//
//  WPTableViewCell.m
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import "WPTableViewCell.h"
#import "WPDeviceIdentification.h"
CGFloat const WPTableviewFixedWidth = 600;

@implementation WPTableViewCell

- (void)setForceCustomCellMargins:(BOOL)forceCustomCellMargins {
    if (_forceCustomCellMargins != forceCustomCellMargins) {
        _forceCustomCellMargins = forceCustomCellMargins;
        [self setClipsToBounds:forceCustomCellMargins];
    }
}

- (void)setFrame:(CGRect)frame {
    if (self.forceCustomCellMargins) {
        CGFloat width = self.superview.frame.size.width;
        if ([WPDeviceIdentification isiPad] && width > WPTableviewFixedWidth) {
            CGFloat x = (width - WPTableviewFixedWidth) / 2;
            
            if (x != frame.origin.x) {
                frame.origin.x += x;
            } else {
                frame.origin.x = x;
            }
            frame.size.width = WPTableviewFixedWidth;
        }
    }
    [super setFrame:frame];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.forceCustomCellMargins) {
        CGFloat width = self.superview.frame.size.width;
        if ([WPDeviceIdentification isiPad] && width > WPTableviewFixedWidth) {
            CGRect frame = self.frame;
            frame.origin.x = (width - WPTableviewFixedWidth) / 2;
            self.frame = frame;
        }
    }
}

@end
