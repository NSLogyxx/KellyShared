//
//  WPNoResultsView.m
//  KellyShared
//
//  Created by share on 2018/12/19.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import "WPNoResultsView.h"
#import <QuartzCore/QuartzCore.h>
#import "WPStyleGuide.h"
#import "WPNUXUtility.h"
#import "WPFontManager.h"
#import "WPDeviceIdentification.h"

@interface WPNoResultsView()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UITextView *messageTextView;
@end

@implementation WPNoResultsView

+ (instancetype)noResultsViewWithTitle:(NSString *)titleText message:(NSString *)messageText accessoryView:(UIView *)accessoryView buttonTitle:(NSString *)buttonTitle {
    WPNoResultsView *view = [WPNoResultsView new];
    view.accessoryView = accessoryView;
    view.titleText = titleText;
    view.messageText = messageText;
    view.buttonTitle = buttonTitle;
    return view;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}

- (void)dealloc {
    self.delegate = nil;
}

- (instancetype)init {
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    [self configureTitleLable];
    [self configureMessageTextView];
    [self configureButton];
}

- (void)configureTitleLable {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
}

- (void)configureMessageTextView {
    self.messageTextView = [[UITextView alloc] init];
    self.messageTextView.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    self.messageTextView.textColor = [WPStyleGuide allTAllShadeGrey];
    self.messageTextView.backgroundColor = [UIColor clearColor];
    self.messageTextView.textAlignment = NSTextAlignmentCenter;
    self.messageTextView.adjustsFontForContentSizeCategory = YES;
    self.messageTextView.editable = NO;
    self.messageTextView.selectable = NO;
    self.messageTextView.clipsToBounds = NO;
    self.messageTextView.textContainerInset = UIEdgeInsetsZero;
    self.messageTextView.textContainer.lineFragmentPadding = 0;
    [self addSubview:self.messageTextView];
}

- (void)configureButton {
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.titleLabel.font = [WPStyleGuide subtitleFontBold];
    self.button.hidden = YES;
    [self.button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.button setTitleColor:[WPStyleGuide wordPressBlue] forState:UIControlStateNormal];
    [self.button setBackgroundImage:[self newButtonBackgroundImage] forState:UIControlStateNormal];
    [self addSubview:self.button];
}

- (void)didMoveToSuperview {
    [self centerInSuperview];
}

- (void)layoutSubviews {
    CGFloat width = 280.f;
    
    [self hideAccessoryViewInNecessary];
    
    _accessoryView.frame = CGRectMake((width - CGRectGetWidth(_accessoryView.frame)) / 2, 0, CGRectGetWidth(_accessoryView.frame), CGRectGetHeight(_accessoryView.frame));
    
    CGSize titleSize = [_titleLabel.text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: _titleLabel.font} context:nil].size;
    
    _titleLabel.frame = CGRectMake(0, (CGRectGetMaxY(_accessoryView.frame) > 0 && _accessoryView.hidden != YES ? CGRectGetMaxY(_accessoryView.frame) + 10.0 : 0), width, titleSize.height);
    
    CGSize messageSize = [_messageTextView.text boundingRectWithSize:CGSizeMake(width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: _messageTextView.font} context:nil].size;
    
    _messageTextView.frame = CGRectMake(0.0f, CGRectGetMaxY(_titleLabel.frame) + 8.0, width, messageSize.height);
    
    [_button sizeToFit];
    CGSize buttonSize = _button.frame.size;
    buttonSize.width += 40;
   
    CGFloat buttonYOrigin = (CGRectGetHeight(_messageTextView.frame) > 0 ? CGRectGetMaxY(_messageTextView.frame) : CGRectGetMaxY(_titleLabel.frame)) + 17.0 ;
    _button.frame = CGRectMake((width - buttonSize.width) / 2, buttonYOrigin, MIN(buttonSize.width, width), buttonSize.height);
    
    CGRect bottomViewRect;
    if (_button != nil) {
        bottomViewRect = _button.frame;
    } else if (_messageTextView.text.length > 0) {
        bottomViewRect = _messageTextView.frame;
    } else if (_titleLabel.text.length > 0) {
        bottomViewRect = _titleLabel.frame;
    } else {
        bottomViewRect = _accessoryView.frame;
    }
    
    CGRect viewFrame = CGRectMake(0, 0, width, CGRectGetMaxY(bottomViewRect));
    self.frame = viewFrame;
    
    if (self.superview) {
        [self centerInSuperview];
    }
}

- (void)resetFonts {
    self.titleText = self.titleLabel.text;
    self.button.titleLabel.font = [WPStyleGuide subtitleFontBold];
}

#pragma mark - Accessory View
- (void)hideAccessoryViewInNecessary {
    UIDevice *device = [UIDevice currentDevice];
    self.accessoryView.hidden = (UIDeviceOrientationIsLandscape(device.orientation) && [WPDeviceIdentification isiPhone]);
}

#pragma mark Helper Methods
- (UIImage *)newButtonBackgroundImage {
    CGRect fillRect = {0, 0, 11.0, 36.0};
    UIEdgeInsets capInsets = {4, 4, 4, 4};
    
    UIGraphicsBeginImageContextWithOptions(fillRect.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [WPStyleGuide wordPressBlue].CGColor);
    CGContextAddPath(context, [UIBezierPath bezierPathWithRoundedRect:CGRectInset(fillRect, 1, 1) cornerRadius:2.0].CGPath);
    
    CGContextStrokePath(context);
    
    UIImage *mainImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [mainImage resizableImageWithCapInsets:capInsets];
}

- (NSAttributedString *)applyMessageStylesToAttributedString:(NSAttributedString *)attributedString {
    NSRange fullTextRange = NSMakeRange(0, attributedString.string.length);
    NSMutableAttributedString *mutableattributedText = [attributedString mutableCopy];
    
    [mutableattributedText addAttribute:NSFontAttributeName value:self.messageTextView.font range:fullTextRange];
    [mutableattributedText addAttribute:NSForegroundColorAttributeName value:self.messageTextView.textColor range:fullTextRange];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.alignment = self.messageTextView.textAlignment;
    [mutableattributedText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:fullTextRange];
    
    return mutableattributedText;
}

#pragma mark - Properties
- (void)setAccessibilityLabel:(NSString *)accessibilityLabel {
    [super setAccessibilityLabel:accessibilityLabel];
    self.isAccessibilityElement = (accessibilityLabel) ? YES : NO;
}

- (NSString *)titleText {
    return  self.titleLabel.text;
}

- (void)setTitleText:(NSString *)titleText {
    if (titleText.length > 0) {
        _titleLabel.attributedText = [[NSAttributedString alloc] initWithString:titleText attributes:[WPNUXUtility titleAttributesWithColor:[WPStyleGuide whisperGrey]]];
    }
    [self setNeedsLayout];
}

- (NSString *)messageText {
    return self.messageTextView.text;
}

- (void)setMessageText:(NSString *)messageText {
    _messageTextView.text = messageText;
    [self setNeedsLayout];
}

- (NSAttributedString *)attributedMessageText {
    return self.messageTextView.attributedText;
}

- (void)setAttributedMessageText:(NSAttributedString *)attributedMessageText {
    NSAttributedString *finalAttributedText = [self applyMessageStylesToAttributedString:attributedMessageText];
    self.messageTextView.attributedText = finalAttributedText;
    self.messageTextView.selectable = YES;
}

- (void)setAccessoryView:(UIView *)accessoryView {
    if (accessoryView == _accessoryView) {
        return;
    }
    [_accessoryView removeFromSuperview];
    _accessoryView = accessoryView;
    if (accessoryView) {
        [self addSubview:accessoryView];
    }
    [self setNeedsDisplay];
}

- (NSString *)buttonTitle {
    return [self.button titleForState:UIControlStateNormal];
}

- (void)setButtonTitle:(NSString *)buttonTitle {
    self.button.hidden = (buttonTitle.length == 0);
    buttonTitle = [buttonTitle uppercaseStringWithLocale:[NSLocale currentLocale]];
    if (buttonTitle.length) {
        [self.button setTitle:buttonTitle forState:UIControlStateNormal];
    }
    [self setNeedsLayout];
}

- (void)showInView:(UIView *)view {
    [view addSubview:self];
    [view bringSubviewToFront:self];
}

- (void)centerInSuperview {
    if (!self.superview) {
        return;
    }
    
    CGRect frame = [self superview].frame;
    
    if ([self.superview.class isSubclassOfClass:[UIScrollView class]]) {
        UIScrollView *scrollView = (UIScrollView *)self.superview;
        CGFloat verticalOffset = scrollView.contentInset.top + scrollView.contentInset.bottom;
        CGFloat horizontalOffset = scrollView.contentInset.left + scrollView.contentInset.right;
        
        if ([self.superview isKindOfClass:[UITableView class]]) {
            UITableView *tableView = (UITableView *)self.superview;
            CGFloat headerHeight = (tableView.tableHeaderView == nil || tableView.tableHeaderView.hidden || tableView.tableHeaderView.alpha == 0) ? 0 : tableView.tableHeaderView.bounds.size.height;
            CGFloat footerHeight = (tableView.tableFooterView == nil || tableView.tableFooterView.hidden || tableView.tableFooterView.alpha == 0) ? 0 : tableView.tableFooterView.bounds.size.height;
            
            verticalOffset += (headerHeight + footerHeight);
            frame.origin.y = headerHeight;
        }
        frame.size.height = frame.size.height - verticalOffset > 0 ? frame.size.height - verticalOffset : frame.size.height;
        frame.size.width = frame.size.width - horizontalOffset > 0 ? frame.size.width - horizontalOffset : frame.size.width;
    }
    
    CGFloat x = (CGRectGetWidth(frame) - CGRectGetWidth(self.frame)) / 2.0;
    CGFloat y = (CGRectGetHeight(frame) / 2.0) - (CGRectGetHeight(self.frame) / 2.0) + frame.origin.y;
    
    frame = self.frame;
    frame.origin.x = x;
    frame.origin.y = y;
    self.frame = frame;
}

#pragma mark - Notification Hanlders
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    [self resetFonts];
    [self setNeedsLayout];
}

- (void)buttonAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTapNoResultsView:)]) {
        [self.delegate didTapNoResultsView:self];
    }
}

@end
