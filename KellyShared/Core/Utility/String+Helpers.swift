//
//  String+Helpers.swift
//  KellyShared
//
//  Created by share on 2018/12/15.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

extension String {
    
    public func stringByDecodingXMLCharacters() -> String {
        return NSString.decodeXMLCharacters(in: self)
    }
    
    public func stringByEncodingXMLCharacters() -> String {
        return NSString.encodeXMLCharacters(in: self)
    }
    
    public func trim() -> String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    public func nonEmptyString() -> String? {
        return isEmpty ? nil : self
    }
    
    public func removing(at index: Index) -> String {
        var copy = self
        copy.remove(at: index)
        return copy
    }
    
    public var characterCount: Int {
        var charCount = 0
        
        if isEmpty == false {
            let textRange = startIndex..<endIndex
            self.enumerateSubstrings(in: textRange, options: [.byWords, .localized]) { (word, _, _, _) in
                let wordLength = word?.count ?? 0
                charCount += wordLength
            }
        }
        return charCount
    }
}

public extension String {
    public mutating func removePrefix(_ prefix: String) {
        if let prefixRange = range(of: prefix), prefixRange.lowerBound == startIndex {
            removeSubrange(prefixRange)
        }
    }
    
    public func removingPrefix(_ prefix: String) -> String {
        var copy = self
        copy.removePrefix(prefix)
        return copy
    }
    
    public mutating func removePrefix(pattern: String, options: NSRegularExpression.Options = []) throws {
        let regexp = try NSRegularExpression(pattern: "^\(pattern)", options: options)
        let fullRange = NSRange(location: 0, length: (self as NSString).length)
        if let match = regexp.firstMatch(in: self, options: [], range: fullRange) {
            let matchRange = match.range
            self = (self as NSString).replacingCharacters(in: matchRange, with: "")
        }
    }
    
    public func removingPrefix(pattern: String, options: NSRegularExpression.Options = []) throws -> String {
        var copy = self
        try copy.removePrefix(pattern: pattern, options: options)
        return copy
    }
}

public extension String {
    public mutating func removeSuffix(_ suffix: String) {
        if let suffixRange = range(of: suffix, options: [.backwards]), suffixRange.upperBound == endIndex {
            self.removeSubrange(suffixRange)
        }
    }
    
    public func removingSuffix(_ suffix: String) -> String {
        var copy = self
        copy.removeSuffix(suffix)
        return copy
    }
    
    public mutating func removeSuffix(pattern: String, options: NSRegularExpression.Options = []) throws {
        let regexp = try NSRegularExpression(pattern: "\(pattern)$", options: options)
        let fullRange = NSRange(location: 0, length: (self as NSString).length)
        if let match = regexp.firstMatch(in: self, options: [], range: fullRange) {
            let matchRange  = match.range
            self = (self as NSString).replacingCharacters(in: matchRange, with: "")
        }
    }
    
    public func removingSuffix(pattern: String, options: NSRegularExpression.Options = []) throws -> String {
        var copy = self
        try copy.removeSuffix(pattern: pattern, options: options)
        return copy
    }
}
