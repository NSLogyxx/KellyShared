//
//  DisplayableImageHelper.h
//  KellyShared
//
//  Created by share on 2018/12/15.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DisplayableImageHelper : NSObject

+ (NSString *)searchPostAttachmentsForImageToDisplay:(NSDictionary *)attachmentsDict existingInContent:(NSString *)content;
+ (NSString *)searchPostContentForImageToDisplay:(NSString *)content;
+ (NSSet *)searchPostContentForAttachmentIdsInGalleries:(NSString *)content;

@end

