//
//  Languages.swift
//  KellyShared
//
//  Created by share on 2018/12/15.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

public class WordPressComLanguageDatabase: NSObject {
    
    public let popular: [Language]
    public let all: [Language]
    public let grouped: [[Language]]
    
    public override init() {
        let path = Bundle(for: WordPressComLanguageDatabase.self).path(forResource: filename, ofType: "json")
        let raw = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let parsed = try! JSONSerialization.jsonObject(with: raw, options: [.mutableContainers, .mutableLeaves]) as? NSDictionary
        
        let parserAll = Language.fromArray(parsed![Keys.all] as! [[String: Any]])
        let parsedPopular = Language.fromArray(parsed![Keys.popular] as! [[String: Any]])
        let merged = parserAll + parsedPopular
        
        popular = parsedPopular
        all = merged.sorted { $0.name < $1.name }
        grouped = [popular] + [all]
    }
    
    public func nameForLanguageWithId(_ languageId: Int) -> String {
        return find(id: languageId)?.name ?? ""
    }
    
    public func find(id: Int) -> Language? {
        return all.first(where: { $0.id == id })
    }
    
    @objc(deviceLanguageId)
    public func deviceLanguageIdNumber() -> NSNumber {
        return NSNumber(value: deviceLanguage.id)
    }
    
    @objc(deviceLanguageSlug)
    public func deviceLanguageSlugString() -> String {
        return deviceLanguage.slug
    }
    
    public var deviceLanguage: Language {
        let variants = LanguageTagVariants(string: deviceLanguageCode)
        for variant in variants {
            if let match = self.languageWithSlug(variant) {
                return match
            }
        }
        return languageWithSlug("en")!
    }
    
    fileprivate func languageWithSlug(_ slug: String) -> Language? {
        let search = languageCodeReplacements[slug] ?? slug
        return all.lazy.filter { $0.slug == search }.first
    }
    
    @objc func _overrideDeviceLanguageCode(_ code: String) {
        deviceLanguageCode = code.lowercased()
    }
    
    public class Language: Equatable {
        public let id: Int
        public let name: String
        public let slug: String
        
        public var description: String {
            return (Locale.current as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: slug) ?? name
        }
        
        init?(dict: [String: Any]) {
            guard let unwrappedId = (dict[Keys.identifier] as? NSNumber)?.intValue,
            let unwrappedSlug = dict[Keys.slug] as? String,
            let unwrappedName = dict[Keys.name] as? String else {
                id = Int.min
                name = String()
                slug = String()
                return nil
            }
            id = unwrappedId
            name = unwrappedName
            slug = unwrappedSlug
        }
        
        public static func fromArray(_ array: [[String: Any]]) -> [Language] {
            return array.compactMap {
                return Language(dict: $0)
            }
        }
        
        public static func == (lhs: Language, rhs: Language) -> Bool {
            return lhs.id == rhs.id
        }
    }
    
    fileprivate lazy var deviceLanguageCode: String = {
        return NSLocale.preferredLanguages.first?.lowercased() ?? "en"
    }()
    
    fileprivate let filename = "Languages"
    
    fileprivate let languageCodeReplacements: [String: String] = [
        "zh-hans": "zh-cn",
        "zh-hant": "zh-tw"
    ]
    
    fileprivate struct Keys {
        static let popular = "popular"
        static let all = "all"
        static let identifier = "i"
        static let slug = "s"
        static let name = "n"
    }
}

private struct LanguageTagVariants: Sequence {
    let string: String
    
    func makeIterator() -> AnyIterator<String> {
        var components = string.components(separatedBy: "-")
        return AnyIterator {
            guard !components.isEmpty else {
                return nil
            }
            
            let current = components.joined(separator: "-")
            components.removeLast()
            return current
        }
    }
}
