//
//  NSDate+Helpers.swift
//  KellyShared
//
//  Created by share on 2018/12/17.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation
import FormatterKit

extension Date {
    fileprivate struct DateFormatters {
        static let iso8601: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            return formatter
        }()
        
        static let iso8601WithMilliseconds: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            return formatter
        }()
        static let rfc1123: DateFormatter = {
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            return formatter
        }()
        static let mediumDate: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            return formatter
        }()
        static let mediumDateTime: DateFormatter = {
            let formatter = DateFormatter()
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .medium
            formatter.timeStyle = .short
            return formatter
        }()
        static let mediumUTCDateTime: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .short
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            return formatter
        }()
        
        static let longUTCDate: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .long
            formatter.timeStyle = .none
            formatter.timeZone = TimeZone(secondsFromGMT: 0)
            return formatter
        }()
        
        static let shortDateTime: DateFormatter = {
            let formatter = DateFormatter()
            formatter.doesRelativeDateFormatting = true
            formatter.dateStyle = .short
            formatter.timeStyle = .short
            return formatter
        }()
        static let pageSectionFormatter: TTTTimeIntervalFormatter = {
            let formatter = TTTTimeIntervalFormatter()
            formatter.leastSignificantUnit = .day
            formatter.usesIdiomaticDeicticExpressions = true
            formatter.presentDeicticExpression = NSLocalizedString("today", comment: "Today")
            return formatter
        }()
    }
    
    public static func dateWithISO8610String(_ string: String) -> Date? {
        return DateFormatters.iso8601.date(from: string)
    }
    
    public static func dateWithISO8610WithMillisecondsString(_ string: String) -> Date? {
        return DateFormatters.iso8601WithMilliseconds.date(from: string)
    }
    
    public func normalizedDate() -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.autoupdatingCurrent
        
        let flags: NSCalendar.Unit = [.day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(flags, from: self)
        
        var normalized = DateComponents()
        normalized.year = components.year
        normalized.month = components.month
        normalized.weekday = components.weekday
        normalized.day = components.day
        return calendar.date(from: normalized) ?? self
    }
    
    public func toStringAsRFC1123() -> String {
        return DateFormatters.rfc1123.string(from: self)
    }
    
    public func mediumString() -> String {
        let relativeFormatter = TTTTimeIntervalFormatter()
        let absoluteFormatter = DateFormatters.mediumDate
        
        let components = Calendar.current.dateComponents([.day], from: self, to: Date())
        
        if let days = components.day, abs(days) < 7 {
            return relativeFormatter.string(forTimeInterval: timeIntervalSinceNow)
        } else {
            return absoluteFormatter.string(from: self)
        }
    }
    
    public func mediumStringWithTime() -> String {
        return DateFormatters.mediumDateTime.string(from: self)
    }
    
    public func longUTCStringWithoutTime() -> String {
        return DateFormatters.longUTCDate.string(from: self)
    }
    
    public func mediumStringWithUTCTime() -> String {
        return DateFormatters.mediumUTCDateTime.string(from: self)
    }
    
    public func shortStringWithTime() -> String {
        return DateFormatters.shortDateTime.string(from: self)
    }
    
    public func toStringForPageSection() -> String {
        let interval = timeIntervalSinceNow
        
        if interval > 0 && interval < 86400 {
            return NSLocalizedString("later today", comment: "Later today")
        } else {
            return DateFormatters.pageSectionFormatter.string(forTimeInterval: interval)
        }
    }
}

extension NSDate {
    @objc public static func dateWithISO8601String(_ string: String) -> NSDate? {
        return Date.DateFormatters.iso8601.date(from: string) as NSDate?
    }
    
    @objc public func mediumString() -> String {
        return (self as Date).mediumString()
    }
    
    @objc public func mediumStringWithTime() -> String {
        return (self as Date).mediumStringWithTime()
    }
    
    @objc public func shortStringWithTime() -> String {
        return (self as Date).shortStringWithTime()
    }
    
    @objc public func toStringForPageSections() -> String {
        return (self as Date).toStringForPageSection()
    }
}
