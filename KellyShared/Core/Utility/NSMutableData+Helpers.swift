//
//  NSMutableData+Helpers.swift
//  KellyShared
//
//  Created by share on 2018/12/17.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

extension NSMutableData {
    @objc public func appendString(_ string: String) {
        if let data = string.data(using: String.Encoding.utf8) {
            append(data)
        }
    }
}
