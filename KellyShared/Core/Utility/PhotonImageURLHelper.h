//
//  PhotonImageURLHelper.h
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhotonImageURLHelper : NSObject

+ (NSURL *)photonURLWithSize:(CGSize)size forImageURL:(NSURL *)url;
+ (NSURL *)photonURLWithSize:(CGSize)size forImageURL:(NSURL *)url forceResize:(BOOL)forceResize imageQuality:(NSUInteger)quality;

@end

NS_ASSUME_NONNULL_END
