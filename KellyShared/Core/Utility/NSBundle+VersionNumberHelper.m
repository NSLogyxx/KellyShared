//
//  NSBundle+VersionNumberHelper.m
//  KellyShared
//
//  Created by share on 2018/12/17.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import "NSBundle+VersionNumberHelper.h"

@implementation NSBundle (VersionNumberHelper)

- (NSString *)detailedVersionNumber {
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *versionNumberString = [NSString stringWithFormat:@"%@ (%@)", [mainBundle.infoDictionary objectForKey:@"CFBundleShortVersionString"], [mainBundle.infoDictionary objectForKey:@"CFBundleVersion"]];
    return versionNumberString;
}

- (NSString *)shortVersionSring {
    NSString *appVersion = [NSBundle.mainBundle.infoDictionary objectForKey:@"CFBundleShortVersionString"];
#if DEBUG
    appVersion = [appVersion stringByAppendingString:@" (DEV)"];
#endif
    return appVersion;
}

- (NSString *)bundleVersion {
    NSDictionary *info = [NSBundle mainBundle].infoDictionary;
    return info[(NSString *)kCFBundleVersionKey] ? : [NSString new];
}

@end
