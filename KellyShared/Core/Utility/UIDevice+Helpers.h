//
//  UIDevice+Helpers.h
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (Helpers)

- (NSString *)wordPressIdentifier;

@end

NS_ASSUME_NONNULL_END
