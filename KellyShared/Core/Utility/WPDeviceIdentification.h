//
//  WPDeviceIdentification.h
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WPDeviceIdentification : NSObject

+ (BOOL)isiPhone;
+ (BOOL)isiPad;
+ (BOOL)isRetina;
+ (BOOL)isiPhoneSix;
+ (BOOL)isiPhoneSixPlus;
+ (BOOL)isUnzoomediPhonePlus;
+ (BOOL)isiOSVersionEarlierThan9;
+ (BOOL)isiOSVersionEarlierThan10;
@end

NS_ASSUME_NONNULL_END
