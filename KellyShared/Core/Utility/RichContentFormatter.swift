//
//  RichContentFormatter.swift
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

public class RichContentFormatter {
    public struct RegEx {
        // Forbidden tags
        static let styleTags = try! NSRegularExpression(pattern: "<style[^>]*?>[\\s\\S]*?</style>", options: .caseInsensitive)
        static let scriptTags = try! NSRegularExpression(pattern: "<script[^>]*?>[\\s\\S]*?</script>", options: .caseInsensitive)
        static let gutenbergComments = try! NSRegularExpression(pattern: "<p><!-- /?wp:.+? /?--></p>[\\n]?", options: .caseInsensitive)
        
        // Normalizaing Paragraphs
        static let divTagsStart = try! NSRegularExpression(pattern: "<div[^>]*>", options: .caseInsensitive)
        static let divTagsEnd = try! NSRegularExpression(pattern: "</div>", options: .caseInsensitive)
        static let pTagsStart = try! NSRegularExpression(pattern: "<p[^>]*>\\s*<p[^>]*>", options: .caseInsensitive)
        static let pTagsEnd = try! NSRegularExpression(pattern: "</p>\\s*</p>", options: .caseInsensitive)
        static let newLines = try! NSRegularExpression(pattern: "\\n", options: .caseInsensitive)
        static let preTags = try! NSRegularExpression(pattern: "<pre[^>]*>[\\s\\S]*?</pre>", options: .caseInsensitive)
        
        // Inline Styles
        static let styleAttr = try! NSRegularExpression(pattern: "\\s*style=\"[^\"]*\"", options: .caseInsensitive)
        
        // Gallery Images
        static let galleryImgTags = try! NSRegularExpression(pattern: "<img[^>]*data-orig-file[^>]*/>", options: .caseInsensitive)
        
        // Trailing BR Tags
        static let trailingBRTags = try! NSRegularExpression(pattern: "(\\s*<br\\s*(/?)\\s*>\\s*)+$", options: .caseInsensitive)
        
        // Gutenberg Galleries
        static let gutenbergGalleryList = try! NSRegularExpression(pattern: "(<ul[^>]+>)<li[^>]+gallery-item[^>]+><figure><img .+?</figure></li>", options: .caseInsensitive)
        static let gutenbergGalleryListItem = try! NSRegularExpression(pattern: "<li[^>]+gallery-item[^>]+>(<figure><img .+?</figure>)</li>", options: .caseInsensitive)
    }
    
    public class func formatContentString(_ string: String, isPrivateSite isPrivate: Bool) -> String {
        guard string.count > 0 else {
            return string
        }
        
        var content = string
        content = removeForbiddenTags(content)
        content = normalizeParagraphs(content)
        content = removeInlineStyles(content)
        content = (content as NSString).replacingHTMLEmoticonsWithEmoji() as String
        content = formatGutenbergGallery(content)
        content = resizeGalleryImageURL(content, isPrivateSite: isPrivate)
        
        return content
    }
    
    public class func removeForbiddenTags(_ string: String) -> String {
        guard string.count > 0 else {
            return string
        }
        
        var content = string
        
        content = RegEx.styleTags.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: "")
        
        content = RegEx.styleTags.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: "")
        
         content = RegEx.gutenbergComments.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: "")
        
        return content
    }
    
    public class func normalizeParagraphs(_ string: String) -> String {
        guard string.count > 0 else {
            return string
        }
        
        var content = string
        let openTag = "<p>"
        let closeTag = "</p>"

        content = RegEx.divTagsStart.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: openTag)
        
        content = RegEx.divTagsEnd.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: closeTag)
        
        content = RegEx.pTagsStart.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: openTag)
        
        content = RegEx.pTagsEnd.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: closeTag)
        
        content = filterNewLines(content)
        return content
    }
    
    public class func filterNewLines(_ string: String) -> String {
        var content = string
        
        var ranges = [NSRange]()
        let matches = RegEx.preTags.matches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count))
        
        if matches.count == 0 {
            ranges.append(NSRange(location: 0, length: content.count))
        } else {
            var location = 0
            var length = 0
            for match in matches {
                length = match.range.location - location
                
                let range = NSRange(location: location, length: length)
                ranges.append(range)
                location = match.range.location + match.range.length
            }
            
            length = content.count - location
            ranges.append(NSRange(location: location, length: length))
        }
        
        for range in ranges.reversed() {
            content = RegEx.newLines.stringByReplacingMatches(in: content, options: .reportCompletion, range: range, withTemplate: "")
        }
        return content
    }
    
    public class func removeInlineStyles(_ string: String) -> String {
        guard string.count > 0 else {
            return string
        }
        
        var content = string
        content = RegEx.styleAttr.stringByReplacingMatches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count), withTemplate: "")
        return content
    }
    
    public class func resizeGalleryImageURL(_ string: String, isPrivateSite isPrivate: Bool) -> String {
        guard string.count > 0 else {
            return string
        }
        
        let imageSize = UIScreen.main.bounds.size
        let scale = UIScreen.main.scale
        let scaleSize = imageSize.applying(CGAffineTransform(scaleX: scale, y: scale))
        
        let mContent = NSMutableString(string: string)
        
        let matches = RegEx.galleryImgTags.matches(in: mContent as String, options: [], range: NSRange(location: 0, length: mContent.length))
        
        for match in matches.reversed() {
            let imgElementStr = mContent.substring(with: match.range)
            let srcImgURLStr = parseValueForAttribute("src", inElement: imgElementStr)
            let originalImgURLStr = parseValueForAttribute("data-orig-file", inElement: imgElementStr)
            
            guard let originalURL = URL(string: originalImgURLStr) else {
                continue
            }
            
            var modifiedURL: URL
            
            if isPrivate {
                modifiedURL = WPImageURLHelper.imageURLWithSize(scaleSize, forImageURL: originalURL)
            } else {
                modifiedURL = PhotonImageURLHelper.photonURL(with: imageSize, forImageURL: originalURL)
            }
            
            guard modifiedURL.absoluteString.isEmpty() == false else {
                continue
            }
            
            let mImageStr = NSMutableString(string: imgElementStr)
            mImageStr.replaceOccurrences(of: srcImgURLStr, with: modifiedURL.absoluteString, options: .literal, range: NSRange(location: 0, length: imgElementStr.count))
            mContent.replaceCharacters(in: match.range, with: mImageStr as String)
        }
        return mContent as String
    }
    
    public class func parseValueForAttribute(_ attribute: String, inElement element: String) -> String {
        let elementStr = element as NSString
        var value = ""
        let attrStr = "\(attribute)=\""
        let attrRange = elementStr.range(of: attrStr)
        
        if attrRange.location != NSNotFound {
            let location = attrRange.location + attrRange.length
            let length = elementStr.length - location
            let ending = elementStr.range(of: "\"", options: .caseInsensitive, range: NSRange(location: location, length: length))
            value = elementStr.substring(with: NSRange(location: location, length: ending.location - location))
        }
        return value
    }
    
    public class func removeTrailingBreakTags(_ string: String) -> String {
        guard string.count > 0 else {
            return string
        }
        
        var content = string.trim()
        let matches = RegEx.trailingBRTags.matches(in: content, options: .reportCompletion, range: NSRange(location: 0, length: content.count))
        if let match = matches.first {
            let index = content.index(content.startIndex, offsetBy: match.range.location)
            #if swift(>=4.0)
            content = String(content.prefix(upTo: index))
            #else
            content = content.substring(to: index)
            #endif
        }
        return content
    }
    
    public class func formatGutenbergGallery(_ string: String) -> String {
        let mString = NSMutableString(string: string)
        var matches = RegEx.gutenbergGalleryList.matches(in: mString as String, options: [], range: NSRange(location: 0, length: mString.length))
        for match in matches.reversed() {
            if match.numberOfRanges < 2 {
                continue
            }
            mString.replaceCharacters(in: match.range(at: 1), with: "")
        }
        matches = RegEx.gutenbergGalleryListItem.matches(in: mString as String , options: [], range: NSRange(location: 0, length: mString.length))
        for match in matches.reversed() {
            if match.numberOfRanges < 2 {
                continue
            }
            let image = mString.substring(with: match.range(at: 1))
            mString.replaceCharacters(in: match.range, with: image)
        }
        return mString as String
    }
}
