//
//  WPImageSource.h
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, WPImageSourceError) {
    WPImageSourceErrorUnknown,
    WPImageSourceErrorNilImage
};

extern NSString *const WPImageSourceErrorDomain;

@interface WPImageSource : NSObject

+ (instancetype)sharedSource;

- (void)downloadImageForURL:(NSURL *)url withSuccess:(void (^)(UIImage *image))success failure:(void (^)(NSError *error))failure;

- (void)downloadImageForURL:(NSURL *)url authToken:(NSString *)authToken withSuccess:(void (^)(UIImage *image))success failure:(void (^)(NSError *error))failure;

@end
