//
//  WPMapFilterReduce.h
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id _Nullable (^WPMapBlock) (id _Nonnull obj);
typedef BOOL (^WPFilterBlock) (id _Nullable obj);
typedef id _Nonnull (^WPReduceBlock) (id _Nullable accumulator, id _Nullable obj);

NS_ASSUME_NONNULL_BEGIN

@interface NSArray (WPMapFilterReduce)

- (NSArray *)wp_map:(WPMapBlock)mapBlock;
- (NSArray *)wp_filter:(WPFilterBlock)filterBlock;
- (id)wp_reduce:(WPReduceBlock)reduceBlock withInitialValue:(id)initial;

@end

NS_ASSUME_NONNULL_END
