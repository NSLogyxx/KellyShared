//
//  WPImageURLHelper.swift
//  KellyShared
//
//  Created by share on 2018/12/18.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

public class WPImageURLHelper {
    public class func imageURLWithSize(_ size: CGSize, forImageURL url: URL) -> URL {
        guard var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
            return url
        }
        var newQueryItems = [URLQueryItem]()
        if let queryItems = urlComponents.queryItems {
            for queryItem in queryItems {
                if queryItem.name != "w" && queryItem.name != "h" {
                    newQueryItems.append(queryItem)
                }
            }
        }
        
        let height = Int(size.height)
        let width = Int(size.width)
        
        if height != 0 {
            let heightItem = URLQueryItem(name: "h", value: "\(height)")
            newQueryItems.append(heightItem)
        }
        
        if width != 0 {
            let widthItem = URLQueryItem(name: "w", value: "\(width)")
            newQueryItems.append(widthItem)
        }
        
        urlComponents.queryItems = newQueryItems
        guard let resultURL = urlComponents.url else {
            return url
        }
        return resultURL
    }
}
