//
//  Dictionary+Helpers.swift
//  KellyShared
//
//  Created by share on 2018/12/15.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

extension Dictionary {
    public func valueAsString(forKey key: Key) -> String? {
        let value = self[key]
        switch value {
        case let string as String:
            return string
        case let number as NSNumber:
            return number.description
        default:
            return nil
        }
    }
}
