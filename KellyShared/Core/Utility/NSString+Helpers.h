//
//  NSString+Helpers.h
//  KellyShared
//
//  Created by share on 2018/12/17.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Helpers)

+ (NSString *)makePlainText:(NSString *)string;
+ (NSString *)stripShortcodesFromString:(NSString *)string;
+ (NSString *)summaryFromContent:(NSString *)string;

- (NSString *)stringByUrlEncoding;
- (NSMutableDictionary *)dictionaryFromQueryString;
- (NSString *)stringByReplacingHTMLEmoticonsWithEmoji;
- (NSString *)stringByStrippingHTML;
- (NSString *)stringByEllipsizingWithMaxLength:(NSInteger)lengthlimit preserveWords:(BOOL)preserveWords;
- (BOOL)isWordPressComPath;

- (NSUInteger)wordCount;
- (NSString *)stringByNormalizingWhitespace;
@end
