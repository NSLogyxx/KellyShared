//
//  NSBundle+VersionNumberHelper.h
//  KellyShared
//
//  Created by share on 2018/12/17.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (VersionNumberHelper)

- (NSString *)detailedVersionNumber;
- (NSString *)shortVersionSring;
- (NSString *)bundleVersion;

@end
