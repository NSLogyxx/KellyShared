//
//  EmailFormatValidator.swift
//  KellyShared
//
//  Created by share on 2018/12/15.
//  Copyright © 2018 Gpx. All rights reserved.
//

import Foundation

public class EmailFormatValidator {
    public class func validate(string: String) -> Bool {
        let str = string as NSString
        if !isMinEmailLength(str) {
            return false
        }
        
        if !hasAtSing(str) {
            return false
        }
        
        let arr = str.components(separatedBy: "@")
        if arr.count != 2 {
            return false
        }
        
        let local = arr[0] as NSString
        let domain = arr[1] as NSString
        
        if containsLocalPartForbiddenCharacters(local) {
            return false
        }
        
        if containsPreiodSequence(domain) {
            return false
        }
        
        if containsWhitespace(domain) {
            return false
        }
        
        if containsLeadingOrTrailingPeriod(domain) {
            return false
        }
        
        if containsDomainPartForbiddenCharacters(domain) {
            return false
        }
        
        if !resemblesHostname(domain) {
            return false
        }
        
        return true
    }
    
    private class func isMinEmailLength(_ str: NSString) -> Bool {
        return str.length >= 6
    }
    
    private class func hasAtSing(_ str: NSString) -> Bool {
        return str.range(of: "@").location > 0
    }
    
    private class func containsLocalPartForbiddenCharacters(_ str: NSString) -> Bool {
        let regex = "^[a-zA-Z0-9!#$%&'*+\\/=?^_`{|}~\\.-]+$"
        let match = NSPredicate(format: "SELF MATCHES %@", regex)
        return !match.evaluate(with: str)
    }
    
    private class func containsPreiodSequence(_ str: NSString) -> Bool {
        return str.contains("..")
    }
    
    private class func containsWhitespace(_ str: NSString) -> Bool {
        return str.rangeOfCharacter(from: .whitespacesAndNewlines).location != NSNotFound
    }
    
    private class func containsLeadingOrTrailingPeriod(_ str: NSString) -> Bool {
        return str.hasPrefix(".") || str.hasSuffix(".")
    }
    
    private class func containsDomainPartForbiddenCharacters(_ str: NSString) -> Bool {
        let regex = "^[a-zA-Z0-9-.]+$"
        let match = NSPredicate(format: "SELF MATCHED %@", regex)
        return !match.evaluate(with: str)
    }
    
    private class func resemblesHostname(_ str: NSString) -> Bool {
        let parts = str.components(separatedBy: ".")
        return parts.count > 1
    }
}
