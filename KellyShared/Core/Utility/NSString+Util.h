//
//  NSString+Util.h
//  KellyShared
//
//  Created by share on 2018/12/15.
//  Copyright © 2018 Gpx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSString (Util)

- (bool)isEmpty;
- (NSString *)trim;
- (NSNumber *)numericValue;
- (CGSize)suggestedSizeWithFont:(UIFont *)font width:(CGFloat)width;

@end

@interface NSObject (NumericValueHack)
- (NSNumber *)numericValue;
@end
